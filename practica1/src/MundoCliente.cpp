/*MODIFICADO POR JAVIER ROJO MUÑOZ
GRUPO A404
NUM MATRICULA 51775*/

// Mundo.cpp: implementation of the MundoCliente class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include <sstream>

#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//para fifo:
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include <signal.h>

#include <iostream>


#define MAX_T 3
#define MAX_POINT 3



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


bool ctrl2=false;
struct sigaction act;
void controlador(int n){
	
	switch(n){
	case SIGUSR1:{
		printf("\nHA ACABADO CORRECTAMENTE\n");
		exit(0);
		break;
		} 
	case SIGINT: printf("\nsignal %d\n",n);exit(n); break;
	case SIGPIPE: printf("\nsignal %d\n",n);exit(n); break;
	case SIGTERM: printf("\nsignal %d\n",n);exit(n); break;
	
	case SIGALRM: printf("\nsignal %d\n",n);exit(n);break;
	default: break;	
	}
}
void CMundo::InitSocket(){

	char nombre [30];
	printf("Introduzca nombre: ");
	scanf("%s",nombre);

	//socket(),connect()
	sock_cliente_server.Connect("127.0.0.1",4200);
	//prueba de envio de informacion
	sock_cliente_server.Send(nombre,sizeof(nombre));

}





CMundo::CMundo()
{
	InitSocket();
	Init();
}



CMundo::~CMundo()
{
	//printf("Ejecutando el destructor de cliente\n");

	sh_data_ptr->accion=5;
	munmap(proy,sizeof(sh_data));
	sock_cliente_server.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundo::print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);

	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	t_sincelast+=0.025;

	sock_cliente_server.Receive(cad, sizeof(cad));
	
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %d %d",&esfera.radio, &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.y1, &jugador2.y2, &puntos1, &puntos2);


	

	//implementación del bot
	sh_data_ptr->ctrl2=ctrl2;
	sh_data_ptr->esfera=esfera;
	sh_data_ptr->raqueta1=jugador1;
	sh_data_ptr->raqueta2=jugador2;

	switch (sh_data_ptr->accion){
	case -1:OnKeyboardDown('s',0,0); break;
	
	case 0:OnKeyboardDown('q',0,0); break;
	
	case 1:OnKeyboardDown('w',0,0); break;
		
	default:break;

	}

	if(t_sincelast>100*MAX_T){
		sh_data_ptr->ctrl2=true;
		switch (sh_data_ptr->accion2){
		case 7:OnKeyboardDown('l',0,0); break;
	
		case 8:OnKeyboardDown('p',0,0); break;
	
		case 9:OnKeyboardDown('o',0,0); break;
		
		default:break;
		}	
	}
	else sh_data_ptr->ctrl2=false;
	//fin del juego
	if(puntos1==MAX_POINT || puntos2==MAX_POINT)exit(0);

}


void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{	
	sprintf(cad_th,"%c",key);
	sock_cliente_server.Send(cad_th, sizeof(cad_th));
}

void CMundo::Init()
{


	act.sa_handler=controlador;
	act.sa_flags=0;
	sigaction(SIGALRM,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);

	

	int fdcomp=open("/tmp/comp", O_RDWR | O_CREAT | O_TRUNC ,0777);
	write(fdcomp,&sh_data,sizeof(sh_data));

	proy=(char*)mmap(NULL,sizeof(sh_data),PROT_WRITE | PROT_READ, MAP_SHARED, fdcomp,0);
	close(fdcomp);
	
	sh_data_ptr=(DatosMemCompartida*)proy;


	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;


	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	t_sincelast=0.0f;
	sh_data_ptr->ctrl2=false;
	sh_data_ptr->accion=0;
	sh_data_ptr->accion2=8;
	sh_data_ptr->esfera=esfera;
	sh_data_ptr->raqueta1=jugador1;
	sh_data_ptr->raqueta2=jugador2;

	
}
