/*El logger es el encargado de mostrar los puntos cada vez que
se produzca un 'gol' en el programa tenis. Por ello debe ser
ejecutado primero y abrir la tubería.

En el mundo de tenis también se deben escribir datos en la tubería y
cerrarla apropiadamente.

Javier Rojo Muñoz-A404-51775
*/


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <signal.h>

#define MAX_BUF 2048


struct sigaction act;
int fd,valor=0;
char* miFIFO="/tmp/fifo";

void controlador(int n){
	
	switch(n){
	close(fd);
	unlink(miFIFO);
	case SIGUSR1:{
		printf("\nHA ACABADO SERVER CORRECTAMENTE\n");
		exit(0);
		break;
		} 
	case SIGPIPE: exit(n); break;
	case SIGINT: exit(n); break;
	case SIGTERM: exit(n); break;
	case SIGALRM: exit(n);break;
	default: exit(n); break;	
	}
}




int main(){

    char buf[MAX_BUF];

	act.sa_handler=controlador;
	act.sa_flags=0;
	sigaction(SIGALRM,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);

    if(mkfifo(miFIFO,0666)<0){printf("Error mkfifo desde logger\n");}
    fd=open(miFIFO,O_RDONLY);
    if(fd<0){printf("Error al abrir fifo desde loggerf\n"); }


    while(valor !=-1){
	//valor=read(fd, buf, MAX_BUF);
	if(read(fd,buf,MAX_BUF)<=0)valor=-1;
	else{printf("%s\n", buf);}
	//if(buf[0]=='-' || valor <0)valor=-1;//debemos salir 	
	
    }

    printf("\nFIN DEL JUEGO\n");
    if(close(fd)<0){printf("Error al cerrar fifo desde logger\n");}
    if(unlink(miFIFO)<0){printf("Error al unink fifo desde logger\n");}

    return 0;
}
