#include "DatosMemCompartida.h"

#include <cmath>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#define PRECISION 0.05

int main(){
	char* proy;
	DatosMemCompartida* sh_data_ptr=NULL;
	int fdcomp=-1;
	while(1){
		fdcomp=open("/tmp/comp", O_RDWR);
		if (fdcomp>0)break;
	}

	proy = (char*)mmap(NULL,sizeof(*(sh_data_ptr)), PROT_READ | PROT_WRITE, MAP_SHARED,fdcomp,0);
	sh_data_ptr=(DatosMemCompartida*)proy;
	close(fdcomp);
	
	bool salir=false;
	while(!salir){
		//lógica de movimiento
		
		usleep(25000);
		if(sh_data_ptr->accion==5)salir=true;
		
		float y_centro_1=((sh_data_ptr->raqueta1).y2+(sh_data_ptr->raqueta1).y1)/2;
		if(std::abs( y_centro_1 - (sh_data_ptr->esfera).centro.y) <PRECISION)sh_data_ptr->accion=0;
		else if( y_centro_1 > (sh_data_ptr->esfera).centro.y) sh_data_ptr->accion=-1;
		else if( y_centro_1 < (sh_data_ptr->esfera).centro.y) sh_data_ptr->accion=1;


		if(sh_data_ptr->ctrl2){
			float y_centro_2=((sh_data_ptr->raqueta2).y2+(sh_data_ptr->raqueta2).y1)/2;

			if(std::abs( y_centro_2 - (sh_data_ptr->esfera).centro.y) <PRECISION)sh_data_ptr->accion2=8;
			else if( y_centro_2 > (sh_data_ptr->esfera).centro.y) sh_data_ptr->accion2=7;
			else if( y_centro_2 < (sh_data_ptr->esfera).centro.y) sh_data_ptr->accion2=9;

		}	
	
	}
	munmap(proy,sizeof(*(sh_data_ptr)));	

	return 0;
}
