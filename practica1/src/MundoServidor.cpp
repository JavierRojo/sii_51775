/*MODIFICADO POR JAVIER ROJO MUÑOZ
GRUPO A404
NUM MATRICULA 51775*/

// Mundo.cpp: implementation of the MundoServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include <sstream>

#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//para fifo:
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>


#include <signal.h>

#include <iostream>


#define MAX_T 3
#define MAX_POINT 3

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



bool thbool=true;

struct sigaction act;
void controlador(int n){
	
	switch(n){
	case SIGUSR1:{
		printf("\nHA ACABADO SERVER CORRECTAMENTE\n");
		exit(0);
		break;
		} 
	case SIGINT: printf("\nsignal %d\n",n);exit(n); break;
	case SIGPIPE: printf("\nsignal %d\n",n);exit(n); break;
	case SIGTERM: printf("\nsignal %d\n",n);exit(n); break;
	case SIGALRM: printf("\nsignal %d\n",n);exit(n);break;
	default: break;	
	}
}

void CMundo::InitSocket(){
	char nombre[30];	

	//socket(),bind(),listen(sock,5)	
	//accept() devuelve un socket.
	sock_server_cliente = sock_server_connect.Accept();

	//Confirmamos que se reciben bien los datos.
	sock_server_cliente.Receive(nombre,sizeof(nombre));
	printf("El nombre del cliente es: %s \n",nombre);

	std::ostringstream oss;
	oss<<"____nuevo juego____\n\n";
	msj =oss.str();
	write(fd, msj.c_str(), msj.length()+1);
}

CMundo::CMundo()
{
	
	InitFifos();
	sock_server_connect.InitServer("127.0.0.1",4200);
	InitSocket();
	Init();
	CreaParedes();
}
CMundo::~CMundo()
{
	//printf("Ejecutando destructor de MundoServer\n");

	std::ostringstream oss;
	oss<<"--fin del juego--\n\n";
	msj =oss.str();
	write(fd, msj.c_str(), msj.length()+1);

	//Cerramos las tuberias
	close(fd);

	thbool=false;
	pthread_join(thid, NULL);
	sock_server_cliente.Close();
	sock_server_connect.Close();

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundo::print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	


	char cad[40];
	sprintf(cad,"Server_Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Server_Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();

	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.radio=0.5;//reestablecemos el radio
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		std::ostringstream oss;
		oss<<"jugador 2 marca 1 punto. Total de jugador 2: "<<puntos2<<" puntos.";
		msj =oss.str();
		write(fd, msj.c_str(), msj.length()+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.radio=0.5;//reestablecemos el radio
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		std::ostringstream oss;
		oss<<"jugador 1 marca 1 punto. Total de jugador 1: "<<puntos1<<" puntos.";
		msj =oss.str();
		write(fd, msj.c_str(), msj.length()+1);
	}

	sprintf(cad,"%f %f %f %f %f %f %f %f %f %d %d",esfera.radio, esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.y1, jugador2.y2, puntos1, puntos2);

	if(sock_server_cliente.Send(cad,sizeof(cad))<0){

		thbool=false;
		pthread_join(thid, NULL);

		sock_server_cliente.Close();
		//sock_server_connect.Close();

		InitSocket();
		Init();

	}

	if(puntos1==MAX_POINT || puntos2==MAX_POINT){

		thbool=false;
		pthread_join(thid, NULL);

		sock_server_cliente.Close();


		InitSocket();
		Init();

	}
}


void* hilo_comandos(void* d){
	CMundo* pt=(CMundo*) d;
	pt->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador(){
	while(thbool){
		usleep(10);
		sock_server_cliente.Receive(cad_th,sizeof(cad_th));
		unsigned char key;
		sscanf(cad_th,"%c",&key);

		if(key =='q')jugador1.velocidad.y= 0;
		if(key =='s')jugador1.velocidad.y=-4;
		if(key =='w')jugador1.velocidad.y= 4;
		if(key =='p')jugador2.velocidad.y= 0;		
		if(key =='l')jugador2.velocidad.y=-4;
		if(key =='o')jugador2.velocidad.y= 4;

	}
	pthread_exit(0);
}




void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{


}
void CMundo::InitFifos(){

	//abrimos la tubería con nombre para logger
	bool err=true;

	miFIFO="/tmp/fifo";
	while(err){
		fd=open(miFIFO.c_str(),O_WRONLY);
		if(fd<0){
			//printf("error abriendo fifo\n");
		}
		else{err=false;}




	act.sa_handler=controlador;

	act.sa_flags=0;

	sigaction(SIGALRM,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);	
	}

}
void CMundo::CreaParedes(){
	Plano p;

	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;
}
void CMundo::Init()
{	
	thbool=true;
	pthread_create(&thid, NULL, hilo_comandos, this);

	puntos1=puntos2=0;
	esfera.centro.x=0;
	esfera.centro.y=0;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	jugador1.velocidad.y=0;


	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	jugador2.velocidad.y=0;

}
